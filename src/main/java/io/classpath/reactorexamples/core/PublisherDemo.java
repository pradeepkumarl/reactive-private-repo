package io.classpath.reactorexamples.core;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.ArrayList;
import java.util.List;

@Component
public class PublisherDemo implements CommandLineRunner {

    @Override
    public void run(String... args) throws Exception {
        Flux<Integer> just = Flux.just(1, 2, 3, 4);
        Mono<Integer> mono = Mono.just(1);
        Publisher<String> publisher = Mono.just("foo");

        List<Integer> elements = new ArrayList<>();
        List<String> elems = new ArrayList<>();

        Flux.just(1, 2, 3, 4)
                .log()
                .subscribe(elements::add);

        Flux.just(1, 2, 3, 4)
                .log()
                .subscribe(new Subscriber<Integer>() {
                    @Override
                    public void onSubscribe(Subscription s) {
                        s.request(Long.MAX_VALUE);
                    }

                    @Override
                    public void onNext(Integer integer) {
                        elements.add(integer);
                    }

                    @Override
                    public void onError(Throwable t) {}

                    @Override
                    public void onComplete() {}
                });


        Flux.just("a", "b", "c", "d")
                .log()
                .subscribe(new Subscriber<String>() {
                    private Subscription s;
                    int onNextAmount;

                    @Override
                    public void onSubscribe(Subscription s) {
                        this.s = s;
                        s.request(2);
                    }

                    @Override
                    public void onNext(String data) {
                        elems.add(data);
                        onNextAmount++;
                        if (onNextAmount % 2 == 0) {
                            s.request(2);
                        }
                    }

                    @Override
                    public void onError(Throwable t) {}

                    @Override
                    public void onComplete() {}
                });

        Flux.just(1, 2, 3, 4)
                .log()
                .map(i -> i * 2)
                .subscribe(elements::add);
    }

    public void fluxErrorHandling(){

        Flux<String> stringFlux = Flux.just("a","b","c")
                .concatWith(Flux.error(new RuntimeException("Exception Occurred")))
                .concatWith(Flux.just("D"))
                .onErrorResume((e) -> { // on error this block gets executed – we are returning a flux on error value
                    System.out.println(e);
                    return Flux.just("default");
                });

        StepVerifier.create(stringFlux.log())
                .expectSubscription()
                .expectNext("a","b","c")
                .expectNext("default")
                .expectError(RuntimeException.class)
                .verify();

    }

}