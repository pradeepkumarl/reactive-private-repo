package io.classpath.reactorexamples.dao;

import io.classpath.reactorexamples.model.Product;
import reactor.core.publisher.Flux;

public class CustomerDAO {

    public Flux<Product> fetchProducts() throws InterruptedException {
        Flux<Product> products = Flux.range(1,50)
                .map(index -> new Product(index, "IPad "+index, Math.ceil(index * Math.random() * 1000)));
        return products;

    }
}