package io.classpath.reactorexamples.core;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;
import reactor.util.retry.Retry;

import java.time.Duration;

public class ExceptionHandlingTests {

    @Test
    public void fluxErrorHandling(){

        Flux<String> stringFlux = Flux.just("a","b","c")
                .concatWith(Flux.error(new RuntimeException("Exception Occurred")))
                .concatWith(Flux.just("D"))
                .onErrorResume((e) -> { // on error this block gets executed – we are returning a flux on error value
                    System.out.println(e);
                    return Flux.just("default");
                });

        StepVerifier.create(stringFlux.log())
                .expectSubscription()
                .expectNext("a","b","c")
                .expectNext("default")
                .expectError(RuntimeException.class)
                .verify();

    }

    @Test
    public void fluxErrorHandling2(){

        Flux<String> stringFlux = Flux.just("a","b","c")
                .concatWith(Flux.error(new RuntimeException("Exception Occurred")))
                .concatWith(Flux.just("D"))
                .onErrorResume((e) -> { // on error this block gets executed – we are returning a flux on error value
                    System.out.println(e);
                    return Flux.just("default");
                });

        StepVerifier.create(stringFlux.log())
                .expectSubscription()
                .expectNext("a","b","c")
                .expectNext("default")
                .verifyComplete();

    }

    @Test
    public void fluxErrorHandling_onErrorReturn(){

        Flux<String> stringFlux = Flux.just("a","b","c")
                .concatWith(Flux.error(new RuntimeException("Exception Occurred")))
                .concatWith(Flux.just("D"))
                .onErrorReturn("default"); // here returning a simple string on any errors

        StepVerifier.create(stringFlux.log())
                .expectSubscription()
                .expectNext("a","b","c")
                .expectNext("default")
                .verifyComplete();
    }

    @Test
    public void fluxErrorHandling_onErrorMap(){

        Flux<String> stringFlux = Flux.just("a","b","c")
                .concatWith(Flux.error(new RuntimeException("Exception Occurred")))
                .concatWith(Flux.just("D"))
                .onErrorMap((e) -> new IllegalArgumentException(e)); // here returning a simple string on any errors

        StepVerifier.create(stringFlux.log())
                .expectSubscription()
                .expectNext("a","b","c")
                .expectError(IllegalArgumentException.class)
                .verify();
    }

    @Test
    public void fluxErrorHandling_withRetry(){

        Flux<String> stringFlux = Flux.just("a","b","c")
                .concatWith(Flux.error(new RuntimeException("Exception Occurred")))
                .concatWith(Flux.just("D"))
                .onErrorMap((e) -> new IllegalArgumentException(e))
                .retryWhen(Retry.backoff(2, Duration.ofSeconds(3)));

        // P.s. Retry produces same stream again

        StepVerifier.create(stringFlux.log())
                .expectSubscription()
                .expectNext("a","b","c")
                .expectNext("a","b","c")
                .expectNext("a","b","c")
                .expectError(IllegalArgumentException.class)
                .verify();
    }

    @Test
    public void fluxErrorHandling_withRetryBackoff(){

        Flux<String> stringFlux = Flux.just("a","b","c")
                .concatWith(Flux.error(new RuntimeException("Exception Occurred")))
                .concatWith(Flux.just("D"))
                .onErrorMap((e) -> new IllegalArgumentException(e))
                .retryWhen(Retry.backoff(2, Duration.ofSeconds(2)));

        StepVerifier.create(stringFlux.log())
                .expectSubscription()
                .expectNext("a","b","c")
                .expectNext("a","b","c")
                .expectNext("a","b","c")
                .expectError(IllegalStateException.class)
                .verify();
    }
}