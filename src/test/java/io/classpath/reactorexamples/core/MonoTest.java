package io.classpath.reactorexamples.core;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

import java.time.Duration;

public class MonoTest {

    @Test
    public void testMono(){
        Mono<String> helloMono = Mono.just("Hello").log();
        helloMono.subscribe(System.out::println);
    }
    @Test
    public void testMonoWithException(){
        Mono<?> helloMono = Mono.just("Hello")
                .then(Mono.error(new RuntimeException("Exception ")))
                .log();
        helloMono.subscribe(System.out::println, (e) -> System.out.println("Came inside the exception block"));
    }

    @Test
    public void testMonoWithDelay() throws InterruptedException {
        Mono.just("one")
                .delayElement(Duration.ofSeconds(1))
                .log()
                .subscribe(data -> System.out.println(data));
        Thread.sleep(5000);
    }

    @Test
    public void schedulerTest(){
        Scheduler schedulerA = Schedulers.newParallel("scheduler-a", 4);
        Scheduler schedulerB = Schedulers.newParallel("scheduler-b", 4);

        Flux.range(1, 2)
                .map(i -> {
                    System.out.println(String.format("First map - (%s), Thread: %s", i, Thread.currentThread().getName()));
                    return i;
                })
                .publishOn(schedulerA)
                .map(i -> {
                    System.out.println(String.format("Second map - (%s), Thread: %s", i, Thread.currentThread().getName()));
                    return i;
                })
                .blockLast();

    }

    @Test
    public void multiplePublish(){

        Scheduler schedulerA = Schedulers.newParallel("scheduler-a", 4);
        Scheduler schedulerB = Schedulers.newParallel("scheduler-b", 4);

        Flux.range(1, 2)
                .map(i -> {
                    System.out.println(String.format("First map - (%s), Thread: %s", i, Thread.currentThread().getName()));
                    return i;
                })
                .publishOn(schedulerA)
                .map(i -> {
                    System.out.println(String.format("Second map - (%s), Thread: %s", i, Thread.currentThread().getName()));
                    return i;
                })
                .publishOn(schedulerB)
                .map(i -> {
                    System.out.println(String.format("Third map - (%s), Thread: %s", i, Thread.currentThread().getName()));
                    return i;
                })
                .blockLast();
    }

    @Test
    public void subscribeOn(){
        Scheduler schedulerA = Schedulers.newParallel("scheduler-a", 4);

        Flux.range(1, 2)
                .map(i -> {
                    System.out.println(String.format("First map - (%s), Thread: %s", i, Thread.currentThread().getName()));
                    return i;
                })
                .subscribeOn(schedulerA)
                .map(i -> {
                    System.out.println(String.format("Second map - (%s), Thread: %s", i, Thread.currentThread().getName()));
                    return i;
                })
                .blockLast();
    }

    @Test
    public void multipleSubscribers(){

        Scheduler schedulerA = Schedulers.newParallel("scheduler-a", 4);
        Scheduler schedulerB = Schedulers.newParallel("scheduler-b", 4);
        Flux.range(1, 2)
                .map(i -> {
                    System.out.println(String.format("First map - (%s), Thread: %s", i, Thread.currentThread().getName()));
                    return i;
                })
                .subscribeOn(schedulerA)
                .map(i -> {
                    System.out.println(String.format("Second map - (%s), Thread: %s", i, Thread.currentThread().getName()));
                    return i;
                })
                .subscribeOn(schedulerB)
                .map(i -> {
                    System.out.println(String.format("Third map - (%s), Thread: %s", i, Thread.currentThread().getName()));
                    return i;
                })
                .blockLast();
    }

    @Test
    public void fluxDelay () throws InterruptedException {
        Flux.just("one", "two", "three")
                .delayElements(Duration.ofSeconds(2))
                .log()
                .subscribe(System.out::println);

        Thread.sleep(5000);
    }


}