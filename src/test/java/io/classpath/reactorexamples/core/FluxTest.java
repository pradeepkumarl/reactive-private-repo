package io.classpath.reactorexamples.core;

import io.classpath.reactorexamples.dao.CustomerDAO;
import io.classpath.reactorexamples.model.Product;
import org.junit.jupiter.api.Test;
import org.reactivestreams.Publisher;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class FluxTest {



    @Test
    public void testFlux(){
        Flux<String> data = Flux.just("Hello Spring Boot");
        data.subscribe(d -> System.out.println(d));
    }


    @Test
    public void testFluxWithMultipleValues(){
        Flux<String> data = Flux.just("Spring Boot", "Hibernate", "JPA", "Microservices")
                .concatWithValues("Classpath-Consulting:: ", "Training dates")
                .log();
        data.subscribe(d -> System.out.println(d));
    }

    @Test
    public void testFluxWithProducts() throws InterruptedException {
        Flux<Product> products = new CustomerDAO().fetchProducts();
        products.subscribe(System.out::println);
    }

    @Test
    public void testCombiningPublishers(){
        Flux<Integer> numbers1 = Flux
                .range(1, 3);

        Flux<Integer> numbers2 = Flux
                .range(4, 2);


        Flux strings = Flux.fromIterable(new ArrayList(
                Arrays.asList("Woolha", "dot", "com")
        ));

        Flux.concat(numbers1, numbers2)
                .subscribe(System.out::println);

    }
    @Test
    public void concatWithPublishers(){
        Flux<Integer> numbers1 = Flux
                .range(1, 3);

        Flux<Integer> numbers2 = Flux
                .range(4, 2);


        Flux strings = Flux.fromIterable(new ArrayList(
                Arrays.asList("Woolha", "dot", "com")
        ));

        numbers1.concatWith(numbers2)
                .subscribe(System.out::println);

    }

    @Test
    public void mergePublishers(){
        Flux<Integer> numbers1 = Flux
                .range(1, 3);

        Flux<Integer> numbers2 = Flux
                .range(4, 2);


        Flux.merge(
                numbers1.delayElements(Duration.ofMillis(150L)),
                numbers2.delayElements(Duration.ofMillis(100L))
        )
                .subscribe(System.out::println);

    }

    @Test
    public void zipPublishers(){
        Flux<Integer> numbers1 = Flux
                .range(1, 3);

        Flux<Integer> numbers2 = Flux
                .range(4, 3);


        //Flux.zip(numbers1, numbers2).subscribe(System.out::println);
        Flux.zip(
                numbers1,
                numbers2,
                (a, b) -> a + b
        )
                .subscribe(System.out::println);


    }

    //Flux with Delay
    @Test
    public void merge() {
        Flux<String> flux1 = Flux.just("Hello", "Vikram");

        flux1 = Flux.interval(Duration.ofMillis(3000))
                .zipWith(flux1, (i, msg) -> msg);


        Flux<String> flux2 = Flux.just("reactive");
        flux2 = Flux.interval(Duration.ofMillis(2000))
                .zipWith(flux2, (i, msg) -> msg);

        Flux<String> flux3 = Flux.just("world");
        Flux.merge(flux1, flux2, flux3)
                .subscribe(System.out::println);

        try {
            Thread.sleep(8000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Test
    public void concat() {
        Flux<String> flux1 = Flux.just("hello").doOnNext(value -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        Flux<String> flux2 = Flux.just("reactive").doOnNext(value -> {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        Flux<String> flux3 = Flux.just("world");
        Flux.concat(flux1, flux2, flux3)
                .map(String::toUpperCase)
                .subscribe(System.out::println);
    }

    @Test
    public void mergeVsConcat(){
        //Lazy subscription of conact
        Flux.concat(Flux.just(1, 2, 3, 4).delayElements(Duration.ofMillis(500)),
                Flux.just(10, 20, 30, 40).delayElements(Duration.ofMillis(500)))
                .subscribe(System.out::println, System.out::println);



//Eager subscription of the merge. Also, try mergeSequential.
        Flux.merge(Flux.range(500, 3).delayElements(Duration.ofMillis(500)),
                Flux.range(-500, 3).delayElements(Duration.ofMillis(300)))
                .subscribe(System.out::println, System.out::println);
    }

    @Test
    public void zipWithFlatMap(){
        Flux<Integer> numbers1 = Flux
                .range(1, 3);

        Flux<Integer> numbers2 = Flux
                .range(4, 3);


        Flux strings = Flux.fromIterable(new ArrayList(
                Arrays.asList("Woolha", "dot", "com")
        ));

        Flux.zip(
                numbers1,
                numbers2,
                strings
        )
                .flatMap(t -> {System.out.println(t); return Mono.empty();})

                .subscribe();
    }

    @Test
    public void zipWithTest(){
        Flux<Integer> numbers1 = Flux
                .range(1, 3);

        Flux<Integer> numbers2 = Flux
                .range(4, 3);
        numbers1.zipWith(numbers2, (a, b) -> a * b)
                .subscribe(System.out::println);
    }

    @Test
    public void testFlatMap(){
        Function<String, String> mapper = String::toUpperCase;

        Flux<String> inFlux = Flux.just("baeldung", ".", "com");
        Flux<String> outFlux = inFlux.map(mapper);

        outFlux.subscribe(d -> System.out.println(d));

        StepVerifier.create(outFlux)
                .expectNext("BAELDUNG", ".", "COM")
                .expectComplete()
                .verify();
    }


    @Test
    public void testFlatMapOperator(){
        Function<String, Publisher<String>> mapper = s -> Flux.just(s.toUpperCase().split(""));

        Flux<String> inFlux = Flux.just("baeldung", ".", "com");
        Flux<String> outFlux = inFlux.flatMap(mapper);
        outFlux.subscribe(d -> System.out.println(d));
        List<String> output = new ArrayList<>();
        outFlux.subscribe(output::add);

    }


    @Test
    public void fluxErrorHandling(){

        Flux<String> stringFlux = Flux.just("a","b","c")
                .concatWith(Flux.error(new RuntimeException("Exception Occurred")))
                .concatWith(Flux.just("D"))
                .onErrorResume((e) -> { // on error this block gets executed – we are returning a flux on error value
                    System.out.println(e);
                    return Flux.just("default");
                });

        StepVerifier.create(stringFlux.log())
                .expectSubscription()
                .expectNext("a","b","c")
                .expectNext("default")
                .expectError(RuntimeException.class)
                .verify();

            }
}